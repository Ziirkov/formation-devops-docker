*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Variables ***
${URL_BUTTON}           http://ec2co-ecsel-1tmk9mv1we7vf-1484179093.us-east-2.elb.amazonaws.com/button.php

*** Test Cases ***
Go to home page
    Wait Until Page Contains    Hello
    Title Should Be     Rage Freebie HTML5 Landing page
